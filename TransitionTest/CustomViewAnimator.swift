//
//  CustomViewAnimator.swift
//  TransitionTest
//
//  Created by Jeyhun Danyalov on 11/4/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CustomViewAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let interval = 0.5
    var forward = true
    var originFrame: CGRect = .zero
    fileprivate var context: UIViewControllerContextTransitioning!
    fileprivate var localView: UIView!
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return interval
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        context = transitionContext
        let containerView = context.containerView
        
        guard let toView = transitionContext.view(forKey: .to) else {return}
        localView = toView
        guard let fromView = transitionContext.view(forKey: .from) else {return}

        let destinationView: UIView!
        var startFrame: CGRect!
        var newPath: CGPath!
        if forward {
            
            destinationView = toView
            containerView.addSubview(destinationView)
            startFrame = originFrame
            newPath = CGPath(rect: destinationView.frame, transform: nil)
            
        } else {
            
            destinationView = fromView
            containerView.insertSubview(toView, belowSubview: destinationView)
            startFrame = destinationView.frame
            newPath = CGPath(rect: originFrame, transform: nil)
            
        }
        
        let layer = CAShapeLayer()
        layer.path = CGPath(rect: startFrame, transform: nil)
        destinationView.layer.mask = layer
        
        let animation = CABasicAnimation(keyPath: "path")
        animation.delegate = self
        animation.fromValue = layer.path
        animation.toValue = newPath
        animation.duration = interval
        animation.timingFunction = CAMediaTimingFunction(name: .easeIn)
        
        layer.path = newPath
        layer.add(animation, forKey: "path")
        
    }
    
}

extension CustomViewAnimator: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        context.completeTransition(!context.transitionWasCancelled)
    }
    
}
