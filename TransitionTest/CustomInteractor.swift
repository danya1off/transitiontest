//
//  CustomInteractor.swift
//  TransitionTest
//
//  Created by Jeyhun Danyaloff on 11/5/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CustomInteractor: UIPercentDrivenInteractiveTransition {
    
    var navigationController: UINavigationController
    private var shoulCompleteTransition = false
    var transitionInProgress = false
    
    init?(for viewController: UIViewController) {
        if let navigation = viewController.navigationController {
            self.navigationController = navigation
            super.init()
            setupBackGesture(for: viewController.view)
        } else {
            return nil
        }
    }
    
    private func setupBackGesture(for view: UIView) {
        let gesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        gesture.edges = .left
        view.addGestureRecognizer(gesture)
    }
    
    @objc private func handleGesture(_ gesture: UIScreenEdgePanGestureRecognizer) {
        
        let viewTranslation = gesture.translation(in: gesture.view?.superview)
        let progress = viewTranslation.x / self.navigationController.view.frame.width
        
        switch gesture.state {
        case .began:
            transitionInProgress = true
            
        case .changed:
            shoulCompleteTransition = progress > 0.3
            update(progress)
            
        case .cancelled:
            transitionInProgress = false
            cancel()
            
        case .ended:
            transitionInProgress = false
            if shoulCompleteTransition {
                navigationController.popViewController(animated: true)
                finish()
            } else {
                cancel()
            }
            
        default:
            return
        }
        
    }
    
}
