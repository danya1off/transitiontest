//
//  ViewController.swift
//  TransitionTest
//
//  Created by Jeyhun Danyalov on 11/4/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    private var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    @objc private func openView() {
        let viewController = TargetViewController()
        viewController.transitioningDelegate = self
        viewController.bgColor = button.backgroundColor ?? .orange
        navigationController?.pushViewController(viewController, animated: true)
        
    }
}

extension MainViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = CustomViewAnimator()
        animator.originFrame = button.frame
        animator.forward = (operation == .push)
        return animator
    }
}

extension MainViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = CustomViewAnimator()
        animator.forward = true
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = CustomViewAnimator()
        animator.forward = false
        return animator
    }
}

extension MainViewController {
    
    fileprivate func setup() {
        view.backgroundColor = .white
        navigationController?.delegate = self
        navigationItem.title = "Main view"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        
        button = {
            let button = UIButton(type: .custom)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.backgroundColor = .blue
            button.setTitle("Click", for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.layer.cornerRadius = 5
            button.addTarget(self, action: #selector(openView), for: .touchUpInside)
            return button
        }()
        view.addSubview(button)
        
        button.heightAnchor.constraint(equalToConstant: 60).isActive = true
        button.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
    }
    
}
