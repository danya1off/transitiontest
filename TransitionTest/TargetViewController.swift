//
//  TargetViewController.swift
//  TransitionTest
//
//  Created by Jeyhun Danyalov on 11/4/18.
//  Copyright © 2018 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TargetViewController: UIViewController {
    
    var bgColor = UIColor.orange
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

extension TargetViewController {
    
    fileprivate func setup() {
        view.backgroundColor = bgColor
        navigationItem.title = "Target view"
        
        let label: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "BLA BLA"
            label.textColor = .white
            label.font = UIFont.boldSystemFont(ofSize: 18)
            return label
        }()
        view.addSubview(label)
        
        let label2: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "Hello from target view :)"
            label.textColor = .white
            label.font = UIFont.boldSystemFont(ofSize: 18)
            return label
        }()
        view.addSubview(label2)
        
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 150).isActive = true
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        label2.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label2.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
}
